import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://myid.buu.ac.th/')

WebUI.setText(findTestObject('Object Repository/Page_My ID/input_(Username)_user (7)'), '62160281')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_My ID/input_(Password)_pass (7)'), 'EcJ5Mbh3llAle/QcCzA7io1eaHHKugie')

WebUI.click(findTestObject('Object Repository/Page_My ID/button_Sign in (7)'))

WebUI.click(findTestObject('Object Repository/Page_My ID/a_Change PasswordRenew password (5)'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_My ID/input_(New Password)_newpass (6)'), '3Ucw1c69hmPGA3kX4GJY3cm0HkOHU14DpWa3j5VVRLo=')

WebUI.click(findTestObject('Object Repository/Page_My ID/div_(Re-New Password)                      _11379c'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_My ID/input_(Re-New Password)_renewpass (6)'), '3Ucw1c69hmPGA3kX4GJY3cm0HkOHU14DpWa3j5VVRLo=')

WebUI.click(findTestObject('Object Repository/Page_My ID/button_Change Password (6)'))

WebUI.click(findTestObject('Object Repository/Page_My ID/div_(         ( )   , - .            _     _538061'))

WebUI.closeBrowser()

